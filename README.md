Conceitos de relacionamento:

ONE TO ONE(UM PARA UM):
Quando a informação de uma tabela depende da informação de uma outra tabela.
Exemplo:
Um Carro vai ter apenas um Chassi e Chassi vai ter apenas um Carro.

ONE TO MANY(UM PARA MUITOS) e MANY TO ONE(MUITOS PARA UM):
Quando a informação de uma tabela depende e tem VARIOS dependentes em outra tabela
e
Quando a informações de uma tabela depende de UMA outra tabela
Exemplo:
Uma Classe de escola tem varios Alunos e cada Aluno está em apenas uma Classe.

MANY TO MANY(MUITOS PARA MUITOS)
Quando a informação de uma tabela está presente mais de uma vez em outra tabela
Uma terceira tabela será responsável em cruzar essas informações
Exemplo:
Uma Musica está cadastrada em varios Albuns musicais e um Album tem varias Musicas
