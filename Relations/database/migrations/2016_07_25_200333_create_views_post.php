<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewsPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('views_post', function (Blueprint $table) {
            $table->increments('id');
            //COMO O RELACIONAMENTO É ONE TO ONE(UM PARA UM), AQUI EU DEFINO A CHAVE ESTRANGIERA
            //QUE SERÁ INCREMENTADA PELO CHAVE id_post representada depois como chave estrangeira
            //UNSIGNED É PASSADO PARA GERAR A FOREING KEY
            $table->integer('id_post')->unsigned();
            //AQUI DIGO QUE É CHAVE ESTRANGEIRA COM REFERENCIA AO id DA TABELA posts
            //E TAMBÉM UM POST DELETADO NÃO TEM MAIS VIEWS, ENTÃO PARA NÃO DEIXAR AQUIVO MORTO
            //E TAMBÉM POR SER INTERESSANTE NO RELACIONAMENTO ONE TO ONE APAGAR O REGISTRO LIGADO
            //APAGAREI EM CASCATA O REGISTRO COM O COMANDO onDelete('cascade')
            $table->foreign('id_post')->references('id')->on('posts')->onDelete('cascade');
            //CHAVE ESTRANGEIRA
            
            $table->integer('qtd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('views_post');
    }
}
