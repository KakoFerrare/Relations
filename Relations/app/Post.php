<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description', 'date', 'time'];
    
    public function qtdViews(){
        //SE ESTE MODEL TIVESSE A PRIMARY KEY DIFERENTE DE ID EXEMPLO id_do_post, EU TERIA QUE PASSAR
        //ELE TAMBEM COMO PARAMETRO NA FUNÇÃO DO THIS:
        //return $this->hasOne('App\PostViews', 'id_post','id_do_post');
        
        //AQUI É O MÉTODO QUE APONTA O RELACIONAMENTO E RETORNA O OBJETO REFERENTE AOS IDS RELACIONADOS
        return $this->hasOne('App\PostViews', 'id_post');
        //ṔODE CHAMAR ASSIM TAMBEM, DA ERRO NO NETBEANS MAS TA CERTO:
        //return $this->hasOne(App\PostViews::class, 'id_post');
    }
    
    public function comments(){
        return $this->hasMany('App\CommentsPost', 'id_post');
    }
    
    public function authors()
    {
        return $this->belongsToMany('App\User', 'users_posts', 'id_post', 'id_user');
    }
}
