<?php

Route::get('/one-to-one', 'RelationshipController@oneToOne');

Route::get('/one-to-many', 'RelationshipController@oneToMany');

Route::get('/many-to-one', 'RelationshipController@manyToOne');

Route::get('/many-to-many', 'RelationshipController@manyToMany');

Route::get('/many-to-many-two', 'RelationshipController@manyToManyTwo');

Route::get('/transaction', 'RelationshipController@transaction');

Route::get('/', function () {
    return view('welcome');
});
