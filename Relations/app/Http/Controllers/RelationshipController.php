<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use App\PostViews;
use App\CommentsPost;
use App\User;
use DB;

class RelationshipController extends Controller
{
    public function oneToOne(){
        /*
        $post = Post::create([
            'title'=>'titulo do post',
            'description'=>'Descrição',
            'date'=> date('Y-m-d'),
            'time'=> date('Hms')
        ]);
        
        $postViews = PostViews::create([
            'qtd'=> 0,
            'id_post'=>$post->id
                ]);
        */
        
        $post = Post::find(7);
        $postViews = $post->qtdViews;
        
        
        /*
        RETORNA O PAI DA VISUALIZAÇÃO
        $view = PostViews::find(1);
        $post = $view->retornaPost;
        dd($post->id);
        */
        $newView = $postViews->qtd +1;
        $update = $postViews->update(['qtd'=>$newView]); 
        dd($update);
        return 'One to One';
    }
    
    public function oneToMany(){
        $post = Post::find(1);
        
        $comments = new CommentsPost([
            'comment'=>'Este post ficou legal'
        ]);
        dd($post->comments()->save($comments));
    }
    
    public function manyToOne(){
        $comment = CommentsPost::find(4);
        $post = $comment->post;
        dd($post);
    }
    
    public function manyToMany(){
        
        $post = Post::find(1);

        /*
        CRIEI NOVO USUÁRIO
        $user = User::create([
            'name'=>'Bolsonaro',
            'password'=> bcrypt(123456),
            'email'=> 'bolsonaro@bossal.com.br'
        ]);
        BUSQUEI UM NOVO USUÁRIO
        $author = User::find(2);
        
        $post->authors()->save($author);
        */
        
        //VINCULA 1 OU MAIS USUARIOS POR ARRAY
        //$post->authors()->attach([1,2]);
        
        //DESVINCULA 1 OU MAIS USUARIOS POR ARRAY
        //$post->authors()->detach(1);
        
        //COM O SYNC SE ALGUM VALOR DO ARRAY QUE ESTAMOS ASSOCIANDO JÁ EXISTIR NO BANCO
        //ELE IGNORA E NÃO DUPLICA
        $post->authors()->sync([1,2]);
        dd($post->authors);
        
    }
    
    public function manyToManyTwo(){
        $author = User::find(2);
        $post = $author->posts()->get();
        dd($post);
        
    }
    
    public function transaction(){
        DB::beginTransaction();
        
        $post = Post::create([
            'title'=>'Post Transaction',
            'description'=>'Transação',
            'date'=> date('Y-m-d'),
            'time'=> date('Hms')
        ]);
        
        $postViews = PostViews::create([
            'qtd'=> 0,
            'id_post'=>$post->id
        ]);
        
        if($post && $postViews){
            echo 'Sucesso';
            DB::commit();
        }else{
            echo 'RollBack';
            DB::rollBack();
        }
        
    }
}
