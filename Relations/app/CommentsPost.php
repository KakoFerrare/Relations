<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsPost extends Model
{
    protected $table = 'comments_post';
    
    protected $fillable = ['id_post', 'comment'];
    
    public function post(){
        return $this->belongsTo('App\Post', 'id_post');
    }
}
