<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostViews extends Model
{
    //DEFINI O NOME DA TABELA POIS ELE NÃO IRIA ENCONTRAR O NOME REFERENTE A CLASSE DESTE MODEL
    protected $table = 'views_post';
    protected $fillable = ['qtd','id_post'];
    public $timestamps = false;
    
    //FUNÇÃO INVERSA DO ONE TO ONE, ELE RETORNA O "PAI" DO OBJETO RELACIONADO, NO CASO AQUI O POST
    public function retornaPost(){
        return $this->belongsTo('App\Post', 'id_post');
    }
}
